'use strict'

const CarModel = use('App/Models/Car')

class TblModelController {
  async index({response}) {
    let data_model = await CarModel.all();
    let total_data = data_model.rows.length;
    if(total_data > 0) {
      return {
        'message': 'success',
        'total': total_data,
        'data': data_model
      }
    } else {
      return {
        'message': 'empty data',
        'total': 0,
        'data': ''
      }
    }
  }
}

module.exports = TblModelController
